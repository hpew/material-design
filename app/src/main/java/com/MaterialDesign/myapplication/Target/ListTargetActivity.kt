package com.MaterialDesign.myapplication.Target

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.support.annotation.RequiresApi
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import com.MaterialDesign.myapplication.AboutUs
import com.MaterialDesign.myapplication.Chat.ChatActivity
import com.MaterialDesign.myapplication.Chat.DataMessage
import com.MaterialDesign.myapplication.IStartTraining
import com.MaterialDesign.myapplication.R
import com.MaterialDesign.myapplication.onItemClick
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.content_list_target.*
import kotlinx.android.synthetic.main.item.*
import java.io.InputStream

@Suppress("UNCHECKED_CAST")
class ListTargetActivity : AppCompatActivity(), IStartTraining{
    val TAG = "JsonFileReader"
    val FILE_TRAINING = "file_training.json"
    lateinit var NICK: String
    private val fileNameArray = arrayOf(
        "natasha_rostova.json",
        "alexandra.json",
        "karina.json",
        "Conversation6.json",
        "Conversation8.json",
        "Conversation9.json",
        "Conversation10.json"
    )
    private var dataFromChat: MutableMap<String, ArrayList<DataMessage>?> = mutableMapOf()
    private val targetInfoList = ArrayList<TargetInfo>()
    private var targetList = ArrayList<Target>()
    private var adapter = TargetAdapter(targetList)

    @TargetApi(Build.VERSION_CODES.M)
    @SuppressLint("ResourceAsColor")
    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        NICK = intent!!.extras?.getString("nick")!!
        setContentView(R.layout.content_list_target)
        checkOnFirstVisit()
        chatResult()
        about_us.setOnClickListener {startActivity(Intent(this, AboutUs::class.java))}
        recycleTarget()
    }

    override fun onResume(){
        chatResult()
        super.onResume()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun checkOnFirstVisit(){
        if(getSharedPreferences(NICK, Context.MODE_PRIVATE).getBoolean("firstOpen", true)){
            val editorSharedPref = getSharedPreferences(NICK, Context.MODE_PRIVATE).edit()
            editorSharedPref.putBoolean(1.toString(), true)
            editorSharedPref.putBoolean("firstOpen", false)
            editorSharedPref.apply()
            startTraining()
        }
    }

    @SuppressLint("PrivateResource")
    @RequiresApi(Build.VERSION_CODES.M)
    override fun startTraining(){
        val jsonTraining = readFileJson(FILE_TRAINING)
        val trainingArray = Gson().fromJson(jsonTraining, TrainingArray::class.java)!!
        training_list_mes.text = trainingArray.training[0].text
        iv_training.tag = "s"
        training.visibility = View.VISIBLE
        training_list_mes.visibility = View.VISIBLE
        iv_training.visibility = View.VISIBLE
        Picasso
            .get()
            .load(R.mipmap.list_anna)
            .placeholder(R.drawable.abc_ab_share_pack_mtrl_alpha)
            .error(android.R.drawable.stat_notify_error)
            .into(iv_profile_image)
        tv_name.text = "Aнна"
        tv_last_message.text = ""
        tv_time.text = ""
        training.setOnClickListener {
            training_list_mes.text = trainingArray.training[1].text
            training.setBackgroundColor(getColor(R.color.black))
            fake_item.visibility = View.VISIBLE
            training.isEnabled = false
        }
        fake_item.setOnClickListener {
            val intent = Intent(this, ChatActivity::class.java).apply {
                putExtra("name", targetList[3].nameTarget)
                putExtra("image", targetList[3].imageChat)
                putExtra("lvl", targetList[3].level)
                putExtra("data", targetInfoList[3].chatInfo)
                putExtra("createdData", dataFromChat[targetList[3].nameTarget])
                putExtra("age", targetInfoList[3].age)
                putExtra("hobby", targetInfoList[3].hobby)
                putExtra("nick", NICK)
                putExtra("training", true)
                putExtra("jsonTraining", jsonTraining)
            }
            startActivity(intent)
            Handler().postDelayed({
                fake_item.isEnabled = false
                fake_item.visibility = View.GONE
                training.visibility = View.GONE
                training_list_mes.visibility = View.GONE
                iv_training.visibility = View.GONE
            }, 3000)
        }
    }

    private fun chatResult(){
        val mapAccess = mutableMapOf<Int, Boolean>()
        val type = object : TypeToken<ArrayList<DataMessage>>(){}.type
        var json: String?
        for(target in targetList){
            json = getSharedPreferences(NICK, Context.MODE_PRIVATE).getString(target.nameTarget, "first")
            if(json != "first"){ dataFromChat[target.nameTarget] = Gson().fromJson(json, type)}
            else {dataFromChat[target.nameTarget] = null}

            mapAccess[target.level] = getSharedPreferences(NICK, Context.MODE_PRIVATE)
                .getBoolean(target.level.toString(), false)
        }
        adapter.mapAccess = mapAccess
        adapter.mapDataMessage = dataFromChat
        adapter.notifyDataSetChanged()
    }

    private fun recycleTarget(){
        val jsonTraining = readFileJson(FILE_TRAINING)
        val trainingArray = Gson().fromJson(jsonTraining, TrainingArray::class.java)!!
        targetList = generateDataTarget(targetInfoList)
        adapter = TargetAdapter(targetList)
        recyle_view_target.adapter = adapter
        recyle_view_target.onItemClick {
            val intent = Intent(this, ChatActivity::class.java).apply {
                putExtra("name", targetList[it].nameTarget)
                putExtra("image", targetList[it].imageChat)
                putExtra("lvl", targetList[it].level)
                putExtra("data", targetInfoList[it].chatInfo)
                putExtra("createdData", dataFromChat[targetList[it].nameTarget])
                putExtra("age", targetInfoList[it].age)
                putExtra("hobby", targetInfoList[it].hobby)
                putExtra("nick", NICK)
                putExtra("training", false)
                putExtra("jsonTraining", jsonTraining)
            }
            startActivity(intent)
        }
    }

    private fun generateDataTarget(targets: List<TargetInfo>?): ArrayList<Target> {
        val imageListTarget = listOf(
            R.mipmap.list_anna,
            R.mipmap.list_alexandra,
            R.mipmap.list_karina,
            R.mipmap.list_d6,
            R.mipmap.list_d8,
            R.mipmap.list_d9,
            R.mipmap.list_d10
        )
        val imageListChat = listOf(
            R.mipmap.anna,
            R.mipmap.alexandra,
            R.mipmap.karina,
            R.mipmap.d6,
            R.mipmap.d8,
            R.mipmap.d9,
            R.mipmap.d10
        )
        var lvl = 0
        val result = ArrayList<Target>()
        for(fileName in fileNameArray) {
            Log.d("TAG_FOR_DEBAG_FILE", fileName)
            targetInfoList.add(Gson().fromJson(readFileJson(fileName), TargetInfo::class.java)!!)
        }
        for(target in targets!!) {
            result += Target(
                target.targetName,
                ++lvl,
                getSharedPreferences(NICK, Context.MODE_PRIVATE).getBoolean(lvl.toString(), false),
                getSharedPreferences(NICK, Context.MODE_PRIVATE).getBoolean(lvl.toString(), false),
                imageListTarget[lvl - 1],
                imageListChat[lvl - 1]
            )
        }
        val sortedResult = result.sortedBy { it.nameTarget }
        result.clear()
        for(item in sortedResult){
            result += item
        }
        val sortInfoList = targetInfoList.sortedBy { it.targetName }
        targetInfoList.clear()
        for(target in sortInfoList){
            targetInfoList += target
        }
        return  result
    }
    private fun readFileJson(fileName: String): String{
        return try {
            val inputStream: InputStream = assets.open(fileName)
            val inputString = inputStream.bufferedReader().use {it.readText()}
            Log.d(TAG,inputString)
            inputString
        } catch (e:Exception){
            Log.d(TAG, e.toString())
            e.toString()
        }
    }

}