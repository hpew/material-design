package com.MaterialDesign.myapplication.Target

import com.MaterialDesign.myapplication.R
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.text.FieldPosition

data class Target(var nameTarget: String = "", var level: Int, var online: Boolean = false, var restart: Boolean = true, var imageTarget: Int? = R.mipmap.qwer, var imageChat:Int? = R.mipmap.qwer): Serializable
data class ChatInfo(val title: String = "", val body: String = ""): Serializable
data class TargetInfo(val targetName: String = "", val age: Int, val hobby: String, val chatInfo: ArrayList<ChatInfo>): Serializable
data class TrainingArray(val training: ArrayList<Training>, val end: ArrayList<Training>): Serializable
data class Training(val position: Int, val src: String, val text: String): Serializable