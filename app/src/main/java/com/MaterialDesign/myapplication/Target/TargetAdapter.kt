package com.MaterialDesign.myapplication.Target

import android.annotation.SuppressLint
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v7.content.res.AppCompatResources.getDrawable
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.MaterialDesign.myapplication.Chat.DataMessage
import com.MaterialDesign.myapplication.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item.view.*

class TargetAdapter(private val items: ArrayList<Target>): RecyclerView.Adapter<TargetAdapter.TargetViewHolder>(){
    var mapDataMessage: MutableMap<String, ArrayList<DataMessage>?>? = null
    var mapAccess: MutableMap<Int, Boolean>? = null
    override fun onCreateViewHolder(parent: ViewGroup, itemsType: Int): TargetViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item, parent, false)
        return TargetViewHolder(itemView)
    }
    override fun getItemCount(): Int = items.size

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(holder: TargetViewHolder, posishion: Int) {
        items[posishion].online = mapAccess?.get(items[posishion].level) ?: false
        holder.bind(items[posishion], mapDataMessage?.get(items[posishion].nameTarget)?.last())
    }

    class TargetViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!){

        private var txtName: TextView? = null
        private var txtMessage: TextView? = null
        private var txtDateOrTime: TextView? = null
        private var imageTarget: ImageView? = null
        init {
            this.txtName = itemView?.findViewById(R.id.tv_name)
            this.txtMessage = itemView?.findViewById(R.id.tv_last_message)
            this.txtDateOrTime = itemView?.findViewById(R.id.tv_time)
            this.imageTarget = itemView?.findViewById(R.id.iv_profile_image)
        }
        //This fun bind data with layout
        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        @SuppressLint("PrivateResource")
        fun bind(item: Target, dataMessage: DataMessage?) {
            if(item.nameTarget == "Анна"){
                itemView.elevation = 15f
            }
            txtName!!.text = item.nameTarget
            txtMessage!!.text = dataMessage?.message
            txtDateOrTime!!.text = dataMessage?.date
            if(item.online) {
                itemView.iv_item_online.background = getDrawable(itemView.context, R.drawable.ic_dot_for_online)
                itemView.ld_item_typing.text = itemView.context.getString(R.string.online)
            }
            else{
                itemView.iv_item_online.background = getDrawable(itemView.context, R.drawable.ic_dot_for_offline)
                itemView.ld_item_typing.text = itemView.context.getString(R.string.offline)
            }
            Picasso
                .get()
                .load(item.imageTarget!!)
                .placeholder(R.drawable.abc_ab_share_pack_mtrl_alpha)
                .error(android.R.drawable.stat_notify_error)
                .into(imageTarget!!)
        }
    }
    //Add items on RecycleView
    fun setItems(tweets: ArrayList<Target>) {
        items.addAll(tweets)
        notifyDataSetChanged()
    }
    //Refresh items
    fun refreshItems(tweets: ArrayList<Target>) {
        items.clear()
        items.addAll(tweets)
        notifyDataSetChanged()
    }
    //Clear RecycleView
    fun clearItems() {
        items.clear()
        notifyDataSetChanged()
    }
    //update access
    fun setAccess(){

    }
}


