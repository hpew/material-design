package com.MaterialDesign.myapplication.User

interface IUser {
    fun validate(): Boolean
    fun userIsFound(nick: String): Boolean
}