package com.MaterialDesign.myapplication.User

import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.provider.MediaStore
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.SpannableStringBuilder
import android.util.Log
import android.view.View
import android.widget.DatePicker
import android.widget.Toast
import com.MaterialDesign.myapplication.ConvertBitmap
import com.MaterialDesign.myapplication.R
import com.MaterialDesign.myapplication.Target.ListTargetActivity
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_registration.*
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

@Suppress(
    "NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS",
    "RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS"
)
class Registration : AppCompatActivity(), IUser, DatePickerDialog.OnDateSetListener{

    var age = 0
    lateinit var currentDateString: String

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        val c = Calendar.getInstance()
        c.set(Calendar.YEAR, year)
        c.set(Calendar.MONTH, month)
        c.set(Calendar.DAY_OF_MONTH, dayOfMonth)

        currentDateString = SimpleDateFormat("dd.MM.yyyy", Locale.UK).format(c.time)
        user_date_of_born.text = SpannableStringBuilder(currentDateString)
        age = DatePickerFragment().getDiffYears(c, Calendar.getInstance())
    }

    private val TAG_REGISTRATION = "registration"
    private val GALLERY_REQUEST = 1
    @SuppressLint("InlinedApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)
        val intent =
            Intent(Intent.ACTION_PICK)
                .setType("image/*")
        user_image.tag = BitmapFactory.decodeResource(resources, R.mipmap.ic_row)

        btn_signup.setOnClickListener{
            signup()
        }

        link_login.setOnClickListener {
            // Finish the registration and return to the Login
            finish()
        }
        user_image.setOnClickListener{
            startActivityForResult(
                intent,
                GALLERY_REQUEST
            )
        }
        user_date_of_born.setOnClickListener {
            val newFragment = DatePickerFragment()
            newFragment.show(supportFragmentManager, "datePicker")

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == GALLERY_REQUEST && resultCode == Activity.RESULT_OK){
            val selectedImage = data?.data
            val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, selectedImage)
            user_image.tag = bitmap
            user_image.setImageBitmap(bitmap)
        }
    }

    override fun validate(): Boolean {
        var valid = true

        val nick = user_nick.text.toString()
        val name = user_name.text.toString()
        if(user_date_of_born.text.toString().isEmpty()){
            user_date_of_born.error = "Введите дату"
            valid = false
        }
        else{
            if(age < 3){
                user_date_of_born.error = "Ограничение по возрасту 3+"
                valid = false
            }
        }

        val description = user_description.text.toString()

        if(nick.isEmpty() || nick.length < 3 || nick.length > 15){
            user_nick.error = "Ник не может иметь меньше 3 или больше 15 символов"
            valid = false
        }
        else if (userIsFound(nick)){
            user_nick.error = "Такой ник уже существует"
            valid = false
        }
        else{
            user_nick.error = null
        }

        if(name.isEmpty() || name.length < 2){
            user_name.error = "Имя не меньше 2 символов"
            valid = false
        }
        else{
            user_name.error = null
        }

        if(description.length > 50){
            user_description.error = "Описание не должно превышать 50 символов"
            valid = false
        }
        else{
            if(description.isEmpty()){
                user_description.text = SpannableStringBuilder("   ")
            }
            user_description.error = null
        }
        return valid
    }

    override fun userIsFound(nick: String): Boolean {
        val isFound = getSharedPreferences(this.getString(R.string.users), Context.MODE_PRIVATE)
            .getString(nick, NOT_FOUND)
        return isFound != NOT_FOUND
    }

    private fun signup(){
        Log.d(TAG_REGISTRATION, "Registration")

        if(!validate()){
            onSignupFailed()
            return
        }

        btn_signup.isEnabled = false
        link_login.isEnabled = false

        progress_bar_registration.visibility = View.VISIBLE

        val nick = user_nick.text.toString()

        createNewUser()

        Handler().postDelayed({
            onSignupSuccess(nick)
            progress_bar_registration.visibility = View.INVISIBLE
        }, 2000)
    }

    private fun createNewUser() {
        val nick = user_nick.text.toString()
        val name = user_name.text.toString()
        val description = user_description.text.toString()
        val image = user_image.tag as Bitmap
        val pixels = ConvertBitmap().getStringFromBitmap(image)
        val json = Gson().toJson(UserData(nick, name, age, currentDateString, description, pixels))

        val editorSharedPref = getSharedPreferences(this.getString(R.string.users), Context.MODE_PRIVATE).edit()
        editorSharedPref.putString(nick, json)
        editorSharedPref.apply()
    }

    private fun onSignupSuccess(nick: String){
        startActivity(
            Intent(this, ListTargetActivity::class.java).apply {
                putExtra("nick", nick)
            }
        )
        Handler().postDelayed({
            finish()
        }, 2000)
        btn_signup.isEnabled = true
        link_login.isEnabled = true
    }

    private fun onSignupFailed(){
        Toast.makeText(baseContext, "Login failed", Toast.LENGTH_LONG).show()
        btn_signup.isEnabled = true
        link_login.isEnabled = true
    }
}
