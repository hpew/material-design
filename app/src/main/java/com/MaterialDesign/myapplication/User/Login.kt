package com.MaterialDesign.myapplication.User

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Toast
import com.MaterialDesign.myapplication.R
import com.MaterialDesign.myapplication.Target.ListTargetActivity
import kotlinx.android.synthetic.main.activity_login.*

class Login : AppCompatActivity(), IUser {
    val TAG_LOGIN = "login"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        btn_login.setOnClickListener {
            login()
        }

        link_signup.setOnClickListener {
            startActivity(Intent(this, Registration::class.java))
        }
    }

    override fun validate(): Boolean {
        var valid = true
        val nick = login_nick.text.toString()

        if(nick.isEmpty() || nick.length < 3 || nick.length > 15){
            login_nick.error = "Ник не может иметь меньше 3 или больше 15 символов"
            valid = false
        }
        else if (!userIsFound(nick)){
            login_nick.error = "Ник не зарегистрирован"
            valid = false
        }
        else{
            login_nick.error = null
        }

        return valid
    }


    override fun userIsFound(nick: String): Boolean {
        val isFound = getSharedPreferences(this.getString(R.string.users), Context.MODE_PRIVATE)
            .getString(nick, NOT_FOUND)
        return isFound != NOT_FOUND
    }

    private fun login(){
        Log.d(TAG_LOGIN, "Login")

        if (!validate()) {
            onLoginFailed()
            return
        }

        btn_login.isEnabled = false
        link_signup.isEnabled = false

        progress_bar_login.visibility = View.VISIBLE

        val nick = login_nick.text.toString()

        Handler().postDelayed({
            onLoginSuccess(nick)
            progress_bar_login.visibility = View.INVISIBLE
        }, 2000)
    }

    private fun onLoginSuccess(nick: String){
        startActivity(
            Intent(this, ListTargetActivity::class.java).apply {
                putExtra("nick", nick)
            }
        )
        Handler().postDelayed({
            finish()
        }, 2000)
        btn_login.isEnabled = true
        link_signup.isEnabled = true
    }

    private fun onLoginFailed(){
        Toast.makeText(baseContext, "Login failed", Toast.LENGTH_LONG).show()
        btn_login.isEnabled = true
        link_signup.isEnabled = true
    }

    override fun onBackPressed() {
        // disable going back to the MainActivity
        moveTaskToBack(true)
    }
}
