package com.MaterialDesign.myapplication.User

import android.app.DatePickerDialog
import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.widget.DatePicker
import java.util.*

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class DatePickerFragment : DialogFragment(){

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        // Use the current date as the default date in the picker
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        // Create a new instance of DatePickerDialog and return it
        return DatePickerDialog(activity, activity as DatePickerDialog.OnDateSetListener, year, month, day)
    }
    fun getDiffYears(first: Calendar, last: Calendar): Int {
        var diff = last.get(Calendar.YEAR) - first.get(Calendar.YEAR)
        if (first.get(Calendar.MONTH) > last.get(Calendar.MONTH)
            || (first.get(Calendar.MONTH) == last.get(Calendar.MONTH)
            && first.get(Calendar.DATE) > last.get(Calendar.DATE))) {
            diff--
        }
        return diff
    }
}