package com.MaterialDesign.myapplication.User

import java.io.Serializable

const val NOT_FOUND = "Oni-Chan please don't inside"

data class UserData(
    val nick: String,
    val name: String,
    val age: Int,
    val dateOfBorn: String,
    val description: String,
    val image: String
): Serializable {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as UserData

        if (nick != other.nick) return false
        if (name != other.name) return false
        if (age != other.age) return false
        if (description != other.description) return false
        if (!image.contentEquals(other.image)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = nick.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + age
        result = 31 * result + description.hashCode()
        return result
    }
}