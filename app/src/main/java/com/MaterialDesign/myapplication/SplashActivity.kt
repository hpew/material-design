package com.MaterialDesign.myapplication

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.view.ViewCompat
import android.support.v7.app.AppCompatActivity
import android.view.animation.AccelerateDecelerateInterpolator
import com.MaterialDesign.myapplication.Target.ListTargetActivity
import com.MaterialDesign.myapplication.User.Login
import kotlinx.android.synthetic.main.layout_splash_screen.*


class SplashActivity: AppCompatActivity() {
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_splash_screen)

        splashAnim()
        supportActionBar!!.title = "Material Design"
    }
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun splashAnim(){
        dd_open.pivotX = dd_open.background.intrinsicWidth.toFloat() / 2f
        dd_open.pivotY = dd_open.background.intrinsicHeight.toFloat()
        dd_open.rotationX = 180f
        ViewCompat.animate(dd_open)
            .rotationX(360f)
            .setDuration(300)
            .setInterpolator(AccelerateDecelerateInterpolator())
            .withEndAction {
                dd_open.elevation = -1f
        ViewCompat.animate(dd_check)
            .translationY(-150f)
            .setStartDelay(200)
            .setDuration(300)
            .setInterpolator(AccelerateDecelerateInterpolator())
            .withEndAction{
                ViewCompat.animate(dd_check)
                    .translationY(0f)
                    .setStartDelay(200)
                    .setDuration(200)
                    .setInterpolator(AccelerateDecelerateInterpolator())
                    .withEndAction{
                        dd_open.elevation = 0f
                ViewCompat.animate(dd_open)
                    .rotationX(180f)
                    .setDuration(300)
                    .setInterpolator(AccelerateDecelerateInterpolator())
                    .withEndAction{
                        startActivity(Intent(this, Login::class.java))
                        finish()
                    }
                    }
            }
            }
    }
}