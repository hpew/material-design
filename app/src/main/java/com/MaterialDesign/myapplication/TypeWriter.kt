package com.MaterialDesign.myapplication

import android.content.Context
import android.os.Handler
import android.util.AttributeSet
import android.widget.TextView

class TypeWriter : TextView {
    private var mText: CharSequence = ""
    private var mIndex: Int = 0
    private var mDelay: Long = 50 // in ms

    constructor(context: Context): super(context)

    constructor(comtext: Context, attrs: AttributeSet) : super(comtext, attrs)

    private val mHandler = Handler()

    private val characterAdder: Runnable = Runnable {
        run {
            text = mText.subSequence(0, mIndex++)

            if (mIndex <= mText.length) {
                mHandler.postDelayed(characterAdder, mDelay)
            }
        }
    }

    fun animateText(txt: CharSequence) {
        mText = txt
        mIndex = 0

        text = ""

        mHandler.removeCallbacks(characterAdder)
        mHandler.postDelayed(characterAdder, mDelay)
    }

    fun getDelay(): Long = mDelay

    fun setCharacterDelay(m: Long){
        mDelay = m
    }

}