package com.MaterialDesign.myapplication

import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.graphics.Rect
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.view.ViewCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import com.r0adkll.slidr.Slidr
import com.r0adkll.slidr.model.SlidrInterface
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.toolbar_fragment.*

class Profile : AppCompatActivity() {

    lateinit var slidr: SlidrInterface
    @SuppressLint("SetTextI18n")
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        fragment_bt_back.setOnClickListener {
            finish()
            overridePendingTransition(0 ,R.anim.slide_out_right)
        }
        account_name.text = getString(R.string.name_profile) + " " + intent.extras?.getString("name")
        account_age.text = getString(R.string.age) + " " + intent.extras?.getInt("age")
        account_hobby.text = getString(R.string.hobby) + " " + intent.extras?.getString("hobby")
        slidr = Slidr.attach(this)
        startAnimation()
        fragment_iv_target_image.setImageDrawable(getDrawable(intent.extras!!.getInt("image")))
        fragment_tv_target_name.text = intent.extras?.getString("name")
        if (intent!!.extras?.getBoolean("training")!!){
            training_profile_mes.text = intent!!.extras?.getString("info")!!
            training_profile.visibility = View.VISIBLE
            training_profile_mes.visibility = View.VISIBLE
            iv_training_profile.visibility = View.VISIBLE
            slidr.lock()
            training_profile.setOnClickListener {
                training_profile.visibility = View.GONE
                training_profile_mes.visibility = View.GONE
                iv_training_profile.visibility = View.GONE
                slidr.unlock()
            }
        }
    }

    @SuppressLint("ObsoleteSdkInt")
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun startAnimation(){
        val ivRect = Rect()
        val displayRect = Rect()
        var firstFlagPaint = true
        fragment_tool_bar.elevation = 2f
        val treeObserver = fragment_tool_bar.viewTreeObserver
        treeObserver.addOnGlobalLayoutListener {
            if (firstFlagPaint) {
                fragment_iv_target_image.getLocalVisibleRect(ivRect)
                fragment_target_account.getLocalVisibleRect(displayRect)
                toolBarAnimation(displayRect, ivRect)
                firstFlagPaint = false
            }
        }
    }

    private fun toolBarAnimation(displayRect: Rect, ivRect: Rect){
        val anim = ValueAnimator.ofInt(fragment_tool_bar.measuredHeight, displayRect.width() + ivRect.width())
        anim.addUpdateListener { valueAnimator ->
            val lav = valueAnimator.animatedValue as Int
            val layoutParams = fragment_tool_bar.layoutParams
            layoutParams.height = lav
            fragment_tool_bar.layoutParams = layoutParams
        }
        anim.duration = 250
        anim.start()
        ViewCompat.animate(fragment_name_online)
            .translationX(fragment_tool_bar.width.toFloat())
            .setDuration(250)
            .setInterpolator(AccelerateDecelerateInterpolator())
            .withStartAction {
                ViewCompat.animate(fragment_iv_target_image)
                    .x(((displayRect.width()  - ivRect.width()) / 2).toFloat())
                    .translationYBy((displayRect.width() / 2).toFloat())
                    .setDuration(250)
                    .setInterpolator(AccelerateDecelerateInterpolator())
                    .withStartAction {
                        ViewCompat.animate(fragment_iv_target_image)
                            .scaleX((displayRect.width()/ivRect.width()).toFloat())
                            .scaleY((displayRect.width()/ivRect.width()).toFloat())
                            .setDuration(250)
                            .setInterpolator(AccelerateDecelerateInterpolator())
                            .withStartAction {}
                    }
            }
    }
}
