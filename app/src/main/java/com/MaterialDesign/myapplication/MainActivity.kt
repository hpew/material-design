package com.MaterialDesign.myapplication

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.view.ViewCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.animation.AccelerateDecelerateInterpolator
import com.MaterialDesign.myapplication.Target.ListTargetActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.content_main.*

@Suppress("DEPRECATION", "NAME_SHADOWING")
class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener{
    @SuppressLint("ResourceAsColor", "NewApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        supportActionBar!!.title = "Material Design"

        val toggle = ActionBarDrawerToggle(
            this,
            drawer_layout, toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()
        main_nav_view.setNavigationItemSelectedListener(this)

        //button click + animation
        button.setOnClickListener {
            eventClickButton()
            button.isClickable = false
        }


    }

    override fun onResume() {
        super.onResume()
        animationBackground()
        button.isClickable = true
    }
    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
//        when (item.itemId) {
//            R.id.nav_camera -> {
//                Toast.makeText(this, article.targetName, Toast.LENGTH_SHORT).show()
//            }
//            R.id.nav_gallery -> {
//                Toast.makeText(this, article.chatInfo[0].body, Toast.LENGTH_SHORT).show()
//            }
//            R.id.nav_slideshow -> {
//                Toast.makeText(this, article.chatInfo[1].colorID!!.toString(), Toast.LENGTH_SHORT).show()
//            }
//            R.id.nav_manage -> {
//                Toast.makeText(this, (article.chatInfo[1].colorID!! + 5).toString(), Toast.LENGTH_SHORT).show()
//            }
//            R.id.nav_share -> {
//                Toast.makeText(this, article.chatInfo[1].title, Toast.LENGTH_SHORT).show()
//            }
//            R.id.nav_send -> {
//
//            }
//        }
//
//        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }
    fun animationBackground() {
        //animation main
        //background
        top_first.translationX -= 500f
        top_first.translationY -= 500f
        top_second.translationX -= 500f
        top_second.translationY -= 500f
        bottom_first.translationX += 500f
        bottom_first.translationY += 500f
        bottom_second.translationX += 500f
        bottom_second.translationY += 500f
        button.scaleX = 0f
        button.scaleY = 0f
        button_dark.alpha= 0f

        ViewCompat.animate(top_first)
            .translationX(0f)
            .translationY(0f)
            .setDuration(500)
            .setInterpolator(AccelerateDecelerateInterpolator())
            .withStartAction {
                ViewCompat.animate(bottom_first)
                    .translationX(0f)
                    .translationY(0f)
                    .setDuration(500).interpolator = AccelerateDecelerateInterpolator()
            }
                    .withEndAction  {
                        ViewCompat.animate(top_second)
                            .translationX(0f)
                            .translationY(0f)
                            .setDuration(500)
                            .setInterpolator(AccelerateDecelerateInterpolator())
                            .withStartAction {
                                ViewCompat.animate(bottom_second)
                                    .translationX(0f)
                                    .translationY(0f)
                                    .setDuration(500)
                                    .setInterpolator(AccelerateDecelerateInterpolator())
                                    .withEndAction {
//                                        button.text = getString(R.string.button_start)
//                                        button.textSize = 20F
                                        //button
                                        ViewCompat.animate(button)
                                            .scaleX(1f)
                                            .scaleY(1f)
                                            .setDuration(500)
                                            .setStartDelay(50)
                                            .withEndAction {
                                                ViewCompat.animate(button_dark)
                                                    .alpha(1f)
                                                    .setDuration(500)
                                                    .setInterpolator(AccelerateDecelerateInterpolator())
                                                    .setStartDelay(50)
                                                    .translationY(35f)
                                        }
                            }
                    }
            }
    }
    fun eventClickButton(){
        ViewCompat.animate(button_dark)
            .alpha(0f)
            .setDuration(500)
            .setInterpolator(AccelerateDecelerateInterpolator())
            .setStartDelay(50)
            .translationY(0f)
            .withEndAction {

                ViewCompat.animate(button)
                    .scaleX(0.8f)
                    .scaleY(0.8f)
                    .setDuration(150)
                    .setInterpolator(AccelerateDecelerateInterpolator())
                    .withEndAction {
                        ViewCompat.animate(button)
                            .scaleX(1.1f)
                            .scaleY(1.1f)
                            .setDuration(150)
                            .setInterpolator(AccelerateDecelerateInterpolator())
                            .withEndAction {
                                ViewCompat.animate(button)
                                    .scaleX(0f)
                                    .scaleY(0f)
                                    .setDuration(150 )
                                    .setInterpolator(AccelerateDecelerateInterpolator())

                                ViewCompat.animate(top_second)
                                    .translationX(-500f)
                                    .translationY(-500f)
                                    .setDuration(300)
                                    .setInterpolator(AccelerateDecelerateInterpolator())
                                    .withLayer()
                                    .withStartAction {

                                        ViewCompat.animate(bottom_second)
                                            .translationX(500f)
                                            .translationY(500f)
                                            .setDuration(300)
                                            .setInterpolator(AccelerateDecelerateInterpolator())
                                    }
                                    .withEndAction {
                                        ViewCompat.animate(top_first)
                                            .translationX(-500f)
                                            .translationY(-500f)
                                            .setDuration(300)
                                            .setInterpolator(AccelerateDecelerateInterpolator())
                                            .withStartAction {

                                                ViewCompat.animate(bottom_first)
                                                    .translationX(500f)
                                                    .translationY(500f)
                                                    .setDuration(300)
                                                    .setInterpolator(AccelerateDecelerateInterpolator())
                                            }
                                            .withEndAction{
                                                val intent = Intent(this, ListTargetActivity::class.java)
                                                startActivity(intent)
                                            }
                                    }
                            }
                    }
            }
    }
}
