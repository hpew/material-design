package com.MaterialDesign.myapplication

import android.content.Context
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v4.view.ViewCompat
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.widget.RelativeLayout
import android.widget.Toast

class AlternatingChildDrawingOrderRelativeLayout : RelativeLayout {
    val TOAST_TEXT = context.getString(R.string.view_is_null)
    // the childDrawingOrder modifier
    private var childDrawingOrderModifier = 0
    private var idChildToFront = 0

    constructor(context: Context) : super(context) {
        init(context)
    }

    private fun init(context: Context) {
//        isClickable = true
//        isChildrenDrawingOrderEnabled = true
//        setOnClickListener{
//                // increment to adjust the child drawing order
//                childDrawingOrderModifier++
//                // call invalidate to redraw with new order modifier
//                ViewCompat.postInvalidateOnAnimation(it)
//
//        }
    }

//    fun toFront(v: View?) {
//        idChildToFront = v.
////        Toast.makeText(context, TOAST_TEXT, Toast.LENGTH_SHORT).show()
//    }

//    override fun getChildDrawingOrder(childCount: Int, i: Int): Int {
//        // increment i with the modifier, then afford for out of bounds using modulus of the child count
//        val returnValue = (i + childDrawingOrderModifier) % childCount
//        Log.v(View.VIEW_LOG_TAG, "getChildDrawingOrder returnValue=$returnValue i=$i")
//        return returnValue
//    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(context)
    }

    constructor(
        context: Context,
        attrs: AttributeSet,
        defStyleAttr: Int
        ) : super(context, attrs, defStyleAttr) {
        init(context)
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    constructor(
        context: Context,
        attrs: AttributeSet,
        defStyleAttr: Int,
        defStyleRes: Int
        ) : super(context, attrs, defStyleAttr, defStyleRes) {
        init(context)
    }

}