package com.MaterialDesign.myapplication

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64
import java.io.ByteArrayOutputStream

class ConvertBitmap {
    public fun getStringFromBitmap(bitmapPicture: Bitmap): String {
        /*
        * This functions converts Bitmap picture to a string which can be
        * JSONified.
        * */
        val COMPRESSION_QUALITY = 100
        val encodedImage: String
        val byteArrayBitmapStream = ByteArrayOutputStream()
        bitmapPicture.compress(
            Bitmap.CompressFormat.PNG, COMPRESSION_QUALITY,
            byteArrayBitmapStream
        )
        val b = byteArrayBitmapStream.toByteArray()
        encodedImage = Base64.encodeToString(b, Base64.DEFAULT)
        return encodedImage
    }

    public fun getBitmapFromString(jsonString: String): Bitmap {
        /*
        * This Function converts the String back to Bitmap
        * */
        val decodedString = Base64.decode(jsonString, Base64.DEFAULT)
        return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
    }
}
