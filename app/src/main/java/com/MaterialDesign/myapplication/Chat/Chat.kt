package com.MaterialDesign.myapplication.Chat

import android.annotation.SuppressLint
import android.util.Log
import com.MaterialDesign.myapplication.R
import com.MaterialDesign.myapplication.Target.ChatInfo
import java.io.Serializable

const val FIRST_WRITTEN = "FirstWritten"
const val TAG: String = "parseMessage"

class Chat(infoChat: ChatInfo): Serializable {
    var index: String = ""
    var message: ArrayList<ArrayList<String>> = ArrayList()
    var autor: String = ""
    @SuppressLint("PrivateResource")
    var image: Int = R.drawable.abc_ic_star_black_16dp
    init {
        this.index = infoChat.title

        if(Regex("""\[\[""").containsMatchIn(infoChat.body))
            this.message.addAll(parseMessage(infoChat.body))
        else {
            this.message.add(arrayListOf("end", infoChat.body))
            autor = "target"
        }

        image = if(autor == "target") R.drawable.splash_screen_image else R.drawable.abc_ic_star_black_16dp

        Log.d(TAG, ".......................................")
        for(mr in message) {
            Log.d(TAG, mr[0])
            Log.d(TAG, mr[1])
        }
    }

    private fun parseMessage(body: String): ArrayList<ArrayList<String>> {
        var counter = 0
        val messageParse= ArrayList<ArrayList<String>>()
        var messageResults: Sequence<MatchResult>? = null
        var indexResults: Sequence<MatchResult>? = null
        when {
            body.startsWith("[[") -> {
                messageResults = Regex("""((?<=\[\[).+(?=\|))""").findAll(body)
                indexResults = Regex("""((?<=\|).+(?=\]\]))""").findAll(body)
                autor = "user"
            }
            Regex("""\[\[\|""").containsMatchIn(body) -> {
                messageParse.add(arrayListOf(Regex("""((?<=\|).+(?=\]\]))""").find(body)!!.value,
                    Regex(""".+(?=\n\[\[)""").find(body)!!.value))
                autor = "target"
            }
            Regex(""".+(?=\n\[\[)""").containsMatchIn(body) -> {
                messageParse.add(arrayListOf(
                    FIRST_WRITTEN,
                    Regex(""".+(?=\n\[\[)""").find(body)!!.value))
                messageResults = Regex("""((?<=\[\[).+(?=\|))""").findAll(body)
                indexResults = Regex("""((?<=\|).+(?=\]\]))""").findAll(body)
                autor = "user"
            }
        }
        if (messageResults != null) {
            for (messageResult in messageResults) {
                messageParse.add(arrayListOf(indexResults!!.elementAt(counter).value, messageResult.value))
                counter++
            }
        }
        return messageParse
    }
}