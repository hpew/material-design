package com.MaterialDesign.myapplication.Chat

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Rect
import android.media.AudioManager
import android.media.SoundPool
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.support.annotation.RequiresApi
import android.support.v4.view.GravityCompat
import android.support.v4.view.ViewCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateDecelerateInterpolator
import android.widget.TextView
import com.MaterialDesign.myapplication.ConvertBitmap
import com.MaterialDesign.myapplication.IStartTraining
import com.MaterialDesign.myapplication.Profile
import com.MaterialDesign.myapplication.R
import com.MaterialDesign.myapplication.Target.ChatInfo
import com.MaterialDesign.myapplication.Target.TrainingArray
import com.MaterialDesign.myapplication.User.NOT_FOUND
import com.MaterialDesign.myapplication.User.UserData
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.r0adkll.slidr.Slidr
import com.r0adkll.slidr.model.SlidrInterface
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_chat.*
import kotlinx.android.synthetic.main.chat_buttons.*
import kotlinx.android.synthetic.main.content_chat.*
import kotlinx.android.synthetic.main.content_chat.chat_buttons
import kotlinx.android.synthetic.main.toolbar_chat.*
import java.io.Serializable
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

@Suppress("CAST_NEVER_SUCCEEDS", "UNCHECKED_CAST")
class ChatActivity : AppCompatActivity(), IStartTraining{
    private lateinit var slidr: SlidrInterface
    private lateinit var NICK: String
    private var training: Boolean = false
    private lateinit var adapter: ChatAdapter//? = null
    private var restart: Boolean = true
    private var online: Boolean = false
    private lateinit var trainingInfo: TrainingArray
    private var positionTraining = 2
    private var firstProfileClick = false
    private var firstAnswerTarget = false

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)
        NICK = intent!!.extras?.getString("nick")!!
        findParameters()
        slidr = Slidr.attach(this)
        online = getSharedPreferences(NICK, Context.MODE_PRIVATE)
            .getBoolean(intent.extras?.getInt("lvl").toString(), false)
        restart = getSharedPreferences(NICK, Context.MODE_PRIVATE)
            .getBoolean(NICK + intent.extras?.getInt("lvl").toString(), true)
        restartAccess()
        navigationBar()
        titleSetting()
        chat()

        if (intent!!.extras?.getBoolean("training")!!){
            training = true
            trainingInfo = Gson().fromJson(intent!!.extras?.getString("jsonTraining"), TrainingArray::class.java)
            startTraining()
            training_toolbar.visibility = View.VISIBLE
            training_chat.visibility = View.VISIBLE
            training_chat_mes.visibility = View.VISIBLE
            iv_training_chat.visibility = View.VISIBLE
            training_chat.setOnClickListener {
                nextTraining()
            }
            iv_fake.setOnClickListener {
                startActivity(
                    Intent(this, Profile::class.java)
                        .apply {
                            putExtra("name", intent.extras?.getString("name"))
                            putExtra("image", intent.extras?.getInt("image"))
                            putExtra("age",intent.extras?.getInt("age"))
                            putExtra("hobby", intent.extras?.getString("hobby"))

                            putExtra("training", training)
                            putExtra("info", trainingInfo.training[positionTraining].text)

                            if(training && !firstProfileClick){
                                firstProfileClick = true

                                nextTraining()
                            }
                        }
                )
                Handler().postDelayed(
                    {
                        iv_fake.visibility = View.GONE
                    },
                    1000
                )
                    nextTraining()
            }
            bt_fake_restart.setOnClickListener {
                bt_restart.visibility = View.INVISIBLE
                ViewCompat.animate(bt_fake_restart)
                    .rotation(-360f)
                    .setDuration(1000)
                    .setInterpolator( AccelerateDecelerateInterpolator())
                    .withStartAction {
                        reset()
                    }
                    .withEndAction {
                        bt_fake_restart.visibility = View.INVISIBLE
                        bt_restart.visibility = View.VISIBLE
                        endTraining()
                    }
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    override fun startTraining() {
        slidr.lock()
        training_chat_mes.text = trainingInfo.training[positionTraining].text

        nextTraining()
    }
    override fun onBackPressed() {
        training = false
        if (drawer_layout_chat.isDrawerOpen(GravityCompat.END)) {
            drawer_layout_chat.closeDrawer(GravityCompat.END)
        } else {
            super.onBackPressed()
        }
    }
    private fun navigationBar(){
        setSupportActionBar(target_tool_bar)
        //TODO
        //    2019-10-12 20:55:41.625 12476-12476/com.example.myapplication E/AndroidRuntime: FATAL EXCEPTION: main
        //    Process: com.example.myapplication, PID: 12476
        //    java.lang.IllegalArgumentException: No drawer view found with gravity LEFT
        //supportActionBar?.setDisplayShowHomeEnabled(true)
        //supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val toggle = object : ActionBarDrawerToggle(
            this,
            drawer_layout_chat,
            target_tool_bar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        ){
            override fun onDrawerOpened(drawerView: View) {
                super.onDrawerOpened(drawerView)
                slidr.lock()
            }

            override fun onDrawerClosed(drawerView: View) {
                super.onDrawerClosed(drawerView)
                slidr.unlock()
            }
        }
        drawer_layout_chat.addDrawerListener(toggle)

        val profileJson = getSharedPreferences(this.getString(R.string.users), Context.MODE_PRIVATE)
            .getString(NICK, NOT_FOUND)
        val profileInfo = Gson().fromJson(profileJson, UserData::class.java)

        val navMenuItems = chat_nav_view.menu
        val profileName  = navMenuItems.findItem(R.id.nav_name)
        val profileAge   = navMenuItems.findItem(R.id.nav_age)
        val profileHobby = navMenuItems.findItem(R.id.nav_hobby)
        val profileDataOfBorn = navMenuItems.findItem(R.id.nav_date_of_born)

        val navHeader = chat_nav_view.getHeaderView(0)
        val profileNick  = navHeader.findViewById<TextView>(R.id.nav_header_nick)
        val profileImage = navHeader.findViewById<CircleImageView>(R.id.nav_header_image)

        profileNick            .text  = profileInfo.nick
        profileName            .title = resources.getString(R.string.name) + profileInfo.name
        profileAge             .title = resources.getString(R.string.age) + profileInfo.age
        profileHobby           .title = resources.getString(R.string.hobby) + profileInfo.description
        profileDataOfBorn      .title = getString(R.string.date_of_born) + profileInfo.dateOfBorn

        val image = ConvertBitmap().getBitmapFromString(profileInfo.image)
        profileImage           .setImageBitmap(image)
    }
    private fun trainingVisible(){
        training_chat.visibility        = View.VISIBLE
        training_chat_mes.visibility    = View.VISIBLE
        iv_training_chat.visibility     = View.VISIBLE
        training_toolbar.visibility     = View.VISIBLE

        ViewCompat.setTranslationZ(training_chat, 1f)
        ViewCompat.setTranslationZ(training_chat_mes, 1f)
        ViewCompat.setTranslationZ(iv_training_chat, 1f)
        ViewCompat.setTranslationZ(chat_buttons, 0f)
        bt_restart.isEnabled = false
        bt_back.isEnabled = false
        iv_target_image.isEnabled = false
        training_chat.isEnabled = true
    }
    private fun btnEnableOff(){
        target_tool_bar.isEnabled = false
        bt_restart.isEnabled = false
        bt_back.isEnabled = false
        iv_target_image.isEnabled = false
        training_chat.isEnabled = false
        chat_buttons.isEnabled = false
    }

    private fun nextTraining(){
        if(training){
            trainingVisible()
            when(positionTraining){
                11 -> {
                    training_chat.visibility = View.INVISIBLE
                    training_chat_mes.visibility = View.INVISIBLE
                    iv_training_chat.visibility = View.INVISIBLE
                    training_toolbar.visibility = View.INVISIBLE
                    training_chat.isEnabled = false
                    return
                }
                -1 -> {
                    training_chat_mes.text = trainingInfo.end[0].text
                    positionTraining = -4
                    return
                }
                -2 -> {
//                    btnEnableOff()
                    training_chat_mes.text = trainingInfo.end[1].text
                    positionTraining = -3
                    return
                }
                -3 -> {
                    btnEnableOff()
                    training_chat_mes.text = trainingInfo.end[2].text
                    bt_fake_restart.visibility = View.VISIBLE
                    return
                }
                -4 -> {
                    endTraining()
                    return
                }
                else -> {
                    when (trainingInfo.training[positionTraining].position){
                        in 5..11 -> {
                            btnEnableOff()
                            training_chat.isEnabled = true
                        }
                        3 ->{
                            iv_fake.isEnabled = true
                            iv_fake.visibility = View.VISIBLE
                            btnEnableOff()
                        }
                        2 ->{
                            ViewCompat.setTranslationZ(chat_buttons, 3f)
                            chat.bringChildToFront(chat_buttons)
                            chat_buttons.bringToFront()
                            chat.invalidate()
                            btnEnableOff()
                            chat_buttons.isEnabled = true
                        }
                    }
                    if (iv_training_chat.tag != trainingInfo.training[positionTraining].src){
                        iv_training_chat.tag = trainingInfo.training[positionTraining].src
                        val poc = when(iv_training_chat.tag){
                            "s" -> R.mipmap.standart
                            "p" -> R.mipmap.popkorm
                            "g" -> R.mipmap.gan
                            else -> R.mipmap.in_hat
                        }
                        iv_training_chat.setImageResource(poc)
                    }
                    if(trainingInfo.training[positionTraining].position in 1..11){
                        training_chat_mes.text = trainingInfo.training[positionTraining].text
                    }
                    positionTraining++
                }
            }

        }
    }
    private fun nextTraining(position: Int){
        if(training){
            training_chat.visibility = View.VISIBLE
            training_chat_mes.visibility = View.VISIBLE
            iv_training_chat.visibility = View.VISIBLE
            training_chat.isEnabled = true
            val poc = when(position)
            {
                -1 -> 0
                -2 -> 1
                else -> 2
            }
            if (iv_training_chat.tag != trainingInfo.end[poc].src){
                iv_training_chat.tag = trainingInfo.end[poc].src
                val pocick = when(iv_training_chat.tag){
                    "s" -> R.mipmap.standart
                    "p" -> R.mipmap.popkorm
                    "g" -> R.mipmap.gan
                    else -> R.mipmap.in_hat
                }
                iv_training_chat.setImageResource(pocick)
            }
            when(position){
                -1 -> {
                    positionTraining = -1
                    nextTraining()
                }
                -2 -> {
                    positionTraining = -2
                    nextTraining()
                }
            }
        }
    }
    private fun endTraining() {
        training = false
        slidr.unlock()
        training_chat.isEnabled = false
        target_tool_bar.isEnabled = false
        bt_restart.isEnabled = true
        bt_back.isEnabled = true
        iv_target_image.isEnabled = true
        training_chat.isEnabled = false
        chat_buttons.isEnabled = true
        training_chat.visibility = View.INVISIBLE
        training_chat_mes.visibility = View.INVISIBLE
        iv_training_chat.visibility = View.INVISIBLE
        training_toolbar.visibility = View.INVISIBLE
    }
    //Save content_chat achievement
    override fun onPause() {
        val json: String = Gson().toJson(adapter.getDataChat())
        val editorSharedPref: SharedPreferences.Editor = getSharedPreferences(NICK, Context.MODE_PRIVATE).edit()
        editorSharedPref.putString(intent.getStringExtra("name"), json)
        editorSharedPref.apply()
        super.onPause()
    }
    //Find and save parameters of parent layout
    private fun findParameters(){
        var firstFlagPaint = true
        val chatTreeObserver = chat.viewTreeObserver
        chatTreeObserver.addOnGlobalLayoutListener {
            if (firstFlagPaint){
                val chatRect = Rect()
                val json: String
                val editorSharedPref: SharedPreferences.Editor = getSharedPreferences(NICK, Context.MODE_PRIVATE).edit()
                chat.getLocalVisibleRect(chatRect)
                json = Gson().toJson(chatRect)
                editorSharedPref.putString("rectLayout", json)
                editorSharedPref.apply()
                firstFlagPaint = false
            }
        }

    }
    //download and inflate content_chat
    @SuppressLint("PrivateResource", "SetTextI18n")
    private fun chat(){
        val chater = generateDataChat(intent.extras!!.getSerializable("data") as ArrayList<ChatInfo>)
        val dataSaves: ArrayList<DataMessage>
        adapter = ChatAdapter()
        recycler_view_chat.adapter = adapter
        val firstChat=
            if(intent.extras!!.getSerializable("createdData") != null) {
                dataSaves = intent.extras!!.getSerializable("createdData") as ArrayList<DataMessage>
                adapter.addElements(dataSaves)
                Log.d("BackDataChat", "content_chat(in): $dataSaves")
                if(dataSaves.last().lastIndex == "end") {
                    btn_chat_left.isEnabled = false
                    btn_chat_right.isEnabled = false
                    btn_chat_left.text = "The "
                    btn_chat_right.text = "End!"
                    return
                }
                searchByIndex(dataSaves.last().lastIndex)
            }
            else {chater[0]}
        write(firstChat, adapter)
        recycler_view_chat.scrollToPosition(adapter.itemCount - 1)
    }

    //save info about restart
    private fun restartAccess(){
        val editorSharedPref = getSharedPreferences(NICK, Context.MODE_PRIVATE).edit()
        editorSharedPref.putBoolean(NICK + intent.extras?.getInt("lvl").toString(), restart)
        editorSharedPref.apply()
        if(restart
            &&
            getSharedPreferences(NICK, Context.MODE_PRIVATE).getBoolean(intent.extras?.getInt("lvl").toString(), false)){
            bt_restart.visibility = View.VISIBLE
        } else{
            bt_restart.visibility = View.INVISIBLE
        }
    }

    //reset data content_chat
    private fun reset(){
        //TODO first lvl completed opened 3
        val rectFromJson = getSharedPreferences(NICK, Context.MODE_PRIVATE).getString("rectLayout", "rect = null")
        val type = object : TypeToken<Rect>(){}.type
        val chatRect = Gson().fromJson<Rect>(rectFromJson, type)
        resetAnimation(chatRect)
        tw_chat.text = resources.getText(R.string.tw_text_chat)
        val chaterRel = generateDataChat(intent.extras!!.getSerializable("data") as ArrayList<ChatInfo>)
        write(chaterRel[0], adapter)
        online = true
        setupAccess()
    }

    private fun resetAnimation(rect: Rect){
        ViewCompat.animate(chat_buttons)
            .translationX(rect.width().toFloat())
            .setDuration(500)
            .setInterpolator( AccelerateDecelerateInterpolator())
            .withStartAction {
                ViewCompat.animate(recycler_view_chat)
                    .translationY((-rect.height()).toFloat())
                    .setDuration(1000)
                    .setInterpolator( AccelerateDecelerateInterpolator())
                    .withEndAction {
                        adapter.clearElements()
                        recycler_view_chat.translationY = 0f
                        bt_restart.rotation = 0f
                        chat_buttons.x -= rect.width().toFloat()
                        chat_buttons.alpha = 0f
                        restartAccess()
                        ViewCompat.animate(chat_buttons)
                            .alpha(1f)
                            .setDuration(1000).interpolator = AccelerateDecelerateInterpolator()
                    }
            }
    }
    //update shared access
    private fun setupAccess(){
        val editorSharedPref = getSharedPreferences(NICK, Context.MODE_PRIVATE).edit()
        editorSharedPref.putBoolean(intent.extras!!.getInt("lvl").toString(), online)
        editorSharedPref.apply()
        if(online){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                iv_check_online.background = getDrawable(R.drawable.ic_dot_for_online)
            }
            ld_typing.text = getString(R.string.online)
        } else{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                iv_check_online.background = getDrawable(R.drawable.ic_dot_for_offline)
            }
            ld_typing.text = getString(R.string.offline)
        }
    }

    //animation typing target's message
    private fun typingAnimation(time: Long){
        var splashTime = 0
        var timeLaps = time
        ld_typing.text = getString(R.string.typing)
        val threadHandler = Handler()
        btn_chat_left.isClickable = false
        btn_chat_right.isClickable = false
        btn_chat_left.isEnabled = false
        btn_chat_right.isEnabled = false
        val runnable = object : Runnable {
            override fun run() {
                threadHandler.postDelayed(this, 100)
                when {
                    splashTime < 250 -> ld_typing.text = "печатает."
                    splashTime in 250..500 -> ld_typing.text = "печатает.."
                    splashTime >= 500 -> ld_typing.text = "печатает..."
                }
                splashTime += 100
                timeLaps -= 100L
                if(splashTime > 750)
                    splashTime = 0
                if(timeLaps <= 0L){
                    ld_typing.text = getString(R.string.online)
                    threadHandler.removeCallbacks(this)
                }
            }
        }
        runnable.run()
    }

    //Well... this is bad. I hope I can fix it.
    private fun write(chat: Chat, adapter: ChatAdapter){
        val mSoundPool = SoundPool(3, AudioManager.STREAM_MUSIC, 0)
        val sound = mSoundPool.load(assets.openFd("chat_mes.mp3"), 0)
        var btnPosition = 0
        val handler = Handler()
        when(chat.autor){
            "user" -> {
                if(chat.message[0][0] == FIRST_WRITTEN){
                    btn_chat_left.isEnabled = false
                    btn_chat_right.isEnabled = false
                    bt_restart.isEnabled = false

                    btnPosition++
                    tw_chat.text = ""
                    tw_chat.animateText(chat.message[0][1])

                    handler.postDelayed({
                        tw_chat.text = resources.getText(R.string.tw_text_chat)
                        adapter.addElement(
                            DataMessage(
                                chat.index,
                                chat.message[0][0],
                                chat.message[0][1],
                                chat.autor
                            ))
                        btn_chat_left.isEnabled = true
                        btn_chat_right.isEnabled = true
                    }, chat.message[0][1].length.toLong() * tw_chat.getDelay() + 500)
                }
                btn_chat_right.text = chat.message[btnPosition][1]
                btn_chat_left.text = chat.message[btnPosition + 1][1]
                btn_chat_left.setOnClickListener{
                    btn_chat_left.isEnabled = false
                    btn_chat_right.isEnabled = false
                    bt_restart.isEnabled = false
                    btnPosition++
                    tw_chat.text = ""
                    tw_chat.animateText(chat.message[btnPosition][1])
                    handler.postDelayed({
                        tw_chat.text = resources.getText(R.string.tw_text_chat)
                        adapter.addElement(
                            DataMessage(
                                chat.index,
                                chat.message[btnPosition][0],
                                chat.message[btnPosition][1],
                                chat.autor
                            ))
                        write(searchByIndex(chat.message[btnPosition][0]), adapter)
                    }, chat.message[btnPosition][1].length.toLong() * tw_chat.getDelay() + 500)
                }
                btn_chat_right.setOnClickListener{
                    btn_chat_left.isEnabled = false
                    btn_chat_right.isEnabled = false
                    bt_restart.isEnabled = false
                    tw_chat.text = ""
                    tw_chat.animateText(chat.message[btnPosition][1])
                    handler.postDelayed({
                        tw_chat.text = resources.getText(R.string.tw_text_chat)
                        adapter.addElement(
                            DataMessage(
                                chat.index,
                                chat.message[btnPosition][0],
                                chat.message[btnPosition][1],
                                chat.autor
                            ))
                        write(searchByIndex(chat.message[btnPosition][0]), adapter)
                    }, chat.message[btnPosition][1].length.toLong() * tw_chat.getDelay() + 500)
                }
                recycler_view_chat.scrollToPosition(adapter.itemCount - 1)
            }
            "target" -> {
                if(training && !firstAnswerTarget){
                    firstAnswerTarget = true
                    nextTraining()
                }
                if(online){
                    if (chat.message[0][0] == "end") {
                        online = false
                        //typingAnimation(chat.message[0][1].length.toLong() * tw_chat.getDelay() + 500, getString(R.string.offline))
                        setupAccess()
                        if(chat.index == "D1S"){
                            nextTraining(-1)
                            val editorSharedPref = getSharedPreferences(NICK, Context.MODE_PRIVATE).edit()
                            editorSharedPref.putBoolean((intent.extras!!.getInt("lvl") + 2).toString(), true)
                            if(intent.extras!!.getInt("lvl") == 1){
                                editorSharedPref.putBoolean((intent.extras!!.getInt("lvl") + 1).toString(), true)
                            }
                            editorSharedPref.apply()
                        }
                        else{
                            nextTraining(-2)
                        }
                        bt_restart.isEnabled = true
                        mSoundPool.play(sound, 1f, 1f, 1, 0, 1f)
                        btn_chat_right.text = chat.message[0][1]
                        btn_chat_left.text = getString(R.string.result)

                        btn_chat_left.isEnabled = false
                        btn_chat_right.isEnabled = false
                        bt_restart.isEnabled = true
                        recycler_view_chat.scrollToPosition(adapter.itemCount - 1)
                        return
                    }
                    else{
                        btn_chat_left.isEnabled = false
                        btn_chat_right.isEnabled = false
                        bt_restart.isEnabled = false
                        typingAnimation(chat.message[0][1].length.toLong() * tw_chat.getDelay() + 500)
                        handler.postDelayed({
                            adapter.addElement(
                                DataMessage(
                                    chat.index,
                                    chat.message[0][0],
                                    chat.message[0][1],
                                    chat.autor
                                )
                            )
                            write(searchByIndex(chat.message[btnPosition][0]), adapter)
                            btn_chat_left.isEnabled = true
                            btn_chat_right.isEnabled = true
                            mSoundPool.play(sound, 1f, 1f, 1, 0, 1f)
                        }, chat.message[0][1].length.toLong() * tw_chat.getDelay() + 500)

                    }
                }
            }
        }

        recycler_view_chat.scrollToPosition(adapter.itemCount - 1)
    }

    //TODO refactor govnocod in function write. Oh..
//    infix fun ChatAdapter.add(content_chat: Chat){
//        addElement(
//            DataMessage(
//                content_chat.message[0][0],
//                content_chat.message[0][1],
//                if (content_chat.autor == "target") content_chat.image else userImage,
//                true,
//                "qeqweqw",
//                content_chat.autor
//            ))
//    }

    //Search by index for next message
    private fun searchByIndex(rightIndex: String): Chat{
        val chater = generateDataChat(intent.extras!!.getSerializable("data") as ArrayList<ChatInfo>)
        loop@for(chat in chater) {if(chat.index == rightIndex) return chat else continue@loop}
        return chater[0]
    }

    //Create settings of toolbar_chat
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    @SuppressLint("ResourceType")
    private fun titleSetting(){
        val targetImage = intent.extras!!.getInt("image")
        val nameTarget: String = intent.getStringExtra("name")
        if(online){
            iv_check_online.background = getDrawable(R.drawable.ic_dot_for_online)
            ld_typing.text = getString(R.string.online)
        } else{
            iv_check_online.background = getDrawable(R.drawable.ic_dot_for_offline)
            ld_typing.text = getString(R.string.offline)
        }
        recycler_view_chat.elevation = -1f
        iv_target_image.setImageDrawable(getDrawable(targetImage))
        iv_fake.setImageDrawable(getDrawable(targetImage))
        tv_target_name.text = nameTarget
        bt_back.setOnClickListener { finish() }
        bt_restart.setOnClickListener {
            ViewCompat.animate(bt_restart)
                .rotation(-360f)
                .setDuration(1000)
                .setInterpolator( AccelerateDecelerateInterpolator())
                .withStartAction {
                    reset()
                    if(!training){restart = false}
                }
        }
        iv_target_image.setOnClickListener {
            startActivity(                Intent(this, Profile::class.java)
                    .apply {
                        putExtra("name", intent.extras?.getString("name"))
                        putExtra("image", intent.extras?.getInt("image"))
                        putExtra("age",intent.extras?.getInt("age"))
                        putExtra("hobby", intent.extras?.getString("hobby"))
                    }
            )
        }
    }

    private fun generateDataChat(chats: ArrayList<ChatInfo>?): ArrayList<Chat> {
        val result = ArrayList<Chat>()
        for(chat in chats!!) {
            result += Chat(chat)
        }
        return result
    }
}

class ChatAdapter: RecyclerView.Adapter<ChatAdapter.ChatViewHolder>() {
    private val messages: ArrayList<DataMessage> = ArrayList()
    private var lastPosition = -1
    override fun onCreateViewHolder(parent: ViewGroup, itemsType: Int): ChatViewHolder{
        val itemView= when(itemsType){
            1 -> {LayoutInflater.from(parent.context).inflate(R.layout.item_chat_user, parent, false)}
            else -> LayoutInflater.from(parent.context).inflate(R.layout.item_chat_target, parent, false)
        }
        return ChatViewHolder(itemView)
    }

    override fun getItemViewType(position: Int): Int= when(messages[position].autor){"user" -> 1 "target" -> 2 else -> 0}

    override fun getItemCount(): Int = messages.size

    override fun onBindViewHolder(holder: ChatViewHolder, @SuppressLint("RecyclerView") position: Int) {
        val itemViewTreeObserver = holder.itemView.viewTreeObserver
        holder.bind(messages[position])
        itemViewTreeObserver.addOnGlobalLayoutListener {

        }
        if(holder.adapterPosition > lastPosition){
            holder.itemView.x += when(messages[position].autor){
                "user" -> 1000f
                "target" -> -1000f
                else -> 0f
            }
            ViewCompat.animate(holder.itemView)
                .translationX(0f)
                .setDuration(1000).interpolator = AccelerateDecelerateInterpolator()
            lastPosition = position
        }
    }

    fun addElement(message: DataMessage){
        messages.add(message)
        notifyDataSetChanged()
    }

    fun addElements(messages: ArrayList<DataMessage>){
        for(message in messages)
            this.messages.add(message)
        lastPosition = -1
        notifyDataSetChanged()
    }

    fun clearElements() {
        messages.clear()
        lastPosition = -1
        notifyDataSetChanged()
    }

    fun getDataChat(): ArrayList<DataMessage>? = if(messages.isNotEmpty()) messages else null

    @SuppressLint("SimpleDateFormat")
    fun getFormattedDate(): String {
        val formatDate = SimpleDateFormat("HH:mm")
        return formatDate.format(Calendar.getInstance().time)
    }

    class ChatViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {
        private var txtMessage: TextView? = null
        private var txtDateOrTime: TextView? = null
        init {
            this.txtMessage      = itemView?.findViewById(R.id.tv_last_message)
            this.txtDateOrTime   = itemView?.findViewById(R.id.tv_time)
        }
        //This fun bind data with layout
        @SuppressLint("PrivateResource")
        fun bind(item: DataMessage) {
            txtMessage?.text        = item.message
            txtDateOrTime?.text     = item.date
        }
    }
}
data class DataMessage(
    val index: String,
    var lastIndex: String,
    val message: String,
    val autor: String,
    val date: String = ChatAdapter().getFormattedDate()): Serializable