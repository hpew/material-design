package com.MaterialDesign.myapplication

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.r0adkll.slidr.Slidr

class AboutUs : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about_us)
        Slidr.attach(this)
        supportActionBar!!.title = getString(R.string.about_us)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }
}
